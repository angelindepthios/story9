from django.db import models

# Create your models here.
class books(models.Model):
    b_id = models.CharField(max_length=10,primary_key=True)
    image = models.TextField()
    title = models.CharField(max_length=200)
    authors = models.CharField(max_length=200,null=True)
    publisher = models.CharField(max_length=200, null=True)
    publisher_date = models.CharField(max_length=200,null=True)
    like = models.IntegerField(default=0)
    def __str__(self):
        return self.title