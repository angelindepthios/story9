from django.test import TestCase,Client,LiveServerTestCase
from django.urls import resolve
from book.views import *
from book.models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time,random,string
from faker import Faker
class story9test(TestCase):
    def test_story9_using_story9_template_search(self):
        responses = Client().get('/')
        self.assertTemplateUsed(responses,'search.html')
    def test_story9_home_function(self):
        found = resolve('/')
        self.assertEqual(found.func,home)
    def test_story9_url_Likedbook_func(self):
        found = resolve('/api/likedbooks/')
        self.assertEqual(found.func,book_like)
    def test_story9_url_unikedbook_func(self):
        found = resolve('/api/unlikedbooks/')
        self.assertEqual(found.func,book_unlike)
    def test_story9_url_topbook_func(self):
        found = resolve('/topbook/')
        self.assertEqual(found.func,topbook)
    def test_model(self):
        add_data = books.objects.create(b_id = '567890uwwe',
            image = 'https://www.google.com/search?q=google+image&safe=strict&rlz=1C1CHWL_enID898ID898&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiXv9PH4pHpAhXQxzgGHULuBUcQ_AUoAXoECBYQAw&biw=626&bih=593#imgrc=-Uf64gZ5_FL4jM',
            title = 'test',
            authors = 'unknown',
            publisher = 'unknown',
            publisher_date = 'unknown',)
        count = books.objects.all().count()
        title = books(title='test')
        self.assertEqual(count,1)
    def test_string_title_model(self):
        b = books.objects.create(title='test')
        self.assertEqual(str(b), 'test')