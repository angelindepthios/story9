from django.urls import path
from .views import home,book_like,book_unlike,topbook
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('', home),
    path('api/likedbooks/', book_like),
    path('api/unlikedbooks/',book_unlike),
    path('topbook/',topbook)]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
