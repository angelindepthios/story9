from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import books
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
def home(request):
    return render(request, 'search.html')
@csrf_exempt
def book_like(request):
    data = json.loads(request.body)
    try:
        book_d = books.objects.get(b_id= data['id'])
        book_d.like +=1
        book_d.save()
    except:
        book_d = books.objects.create(
            b_id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            publisher_date = data['volumeInfo']['publishedDate'],
            like = 1
            )
    return JsonResponse({'like': book_d.like})

@csrf_exempt
def book_unlike(request):
    data = json.loads(request.body)
    try:
        book_d = books.objects.get(b_id= data['id'])
        if book_d.like>0:
            book_d.like -=1
        book_d.save()
    except:
        book_d = books.objects.create(
            b_id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            publisher_date = data['volumeInfo']['publishedDate'],
            )
    return JsonResponse({'unlike': book_d.like})

def topbook(request):
    top_book= books.objects.order_by('-like')
    top_list =[]
    for x in top_book:
       top_list.append({'image':x.image,'title': x.title,'authors':x.authors,'publisher':x.publisher,'published_date':x.publisher_date,'like':x.like})
    return JsonResponse({'top_list':top_list})